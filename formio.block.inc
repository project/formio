<?php
/**
 * @file
 * Block configuration for a form.io form.
 */

/**
 * Implements hook_block_info().
 */
function formio_block_info() {
  $blocks = array();
  // Gets all preset exports and generates a block for each. By passing TRUE,
  // we are ensuring that any new forms that may have been added recently will
  // get indexed as well.
  $forms = formio_load_drupal_forms(TRUE);
  $actions = array('formio', 'entity', 'both');
  foreach ($forms as $key => $form) {
    // Only allow blocks for exports that should be submitted to Form.io or
    // have been stored as entities.
    if (!isset($form->action) || in_array($form->action, $actions)) {
      $blocks['formio__' . $key] = array(
          'info' => t('!form_title', array('!form_title' => 'Formio: ' . $form->drupal_title)),
          'cache' => DRUPAL_NO_CACHE,
      );
    }
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function formio_block_view($delta = '') {
  // Only act on our blocks.
  if (formio_block($delta)) {
    list(, $drupal_name) = explode('__', $delta);
    $form = formio_load_drupal_form($drupal_name);
    $content = array(
      '#theme' => 'formio_form',
      '#form' => (array) $form,
      '#attached' => array(
        'drupal_add_http_header' => formio_add_headers(),
        // This adds seamless to the render.
        'library' => array(
          array('formio', 'seamless'),
        ),
      ),
    );

    // Build the block.
    $block = array();
    $block['title'] = '';
    $block['content'] = $content;

    return $block;
  }

  return FALSE;
}

/**
 * Helper that checks to see if this is one of our blocks.
 *
 * @param string $delta
 *   The block delta.
 *
 * @return bool
 *   If 'formio:' exists in the delta then it is one of ours.
 */
function formio_block($delta) {
  list($name) = explode('__', $delta);
  if ($name === 'formio') {
    return TRUE;
  }
  return FALSE;
}
