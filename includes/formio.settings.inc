<?php
/**
 * @file
 * Customizations used to add additional settings to Form.io forms.
 *
 * Being replaced using the plugin manager and yaml.
 */

/**
 * Helper that find extra settings.
 *
 * @param string $element
 *   The module name or a unique name to set the cache.
 *
 * @return array
 */
function formio_get_extra_settings($element = 'all') {
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['formio_info'] = &drupal_static(__FUNCTION__);
  }
  $formio_info = &$drupal_static_fast['formio_info'];

  if (empty($formio_info)) {
    if ($cache = cache_get("formio_info:extra_settings")) {
      $formio_info = $cache->data;
    }
    else {
      $formio_info = module_invoke_all('formio_extra_settings');
      // Merge in default values.
      foreach ($formio_info as $setting => $data) {
        $formio_info[$setting] += array(
          'callback' => '',
          'validate' => '',
          'submit' => '',
        );
      }
      // Let other modules alter the entity info.
      drupal_alter('formio_extra_settings', $formio_info);
      cache_set("formio_info:extra_settings", $formio_info);
    }
  }

  if ($element === 'all') {
    return $formio_info;
  }
  elseif (isset($formio_info[$element])) {
    return $formio_info[$element];
  }
}

/**
 * Implements hook_formio_extra_settings().
 */
function formio_formio_extra_settings() {
  return array(
    'enable' => array(
      'form' => array(
        '#type' => 'checkbox',
        '#title' => t('Use bootswatch'),
        '#default_value' => 0,
        '#states' => array(
          'invisible' => array(
            ':input[name="_id"]' => array('value' => '_none'),
          ),
        ),
        '#ajax' => array(
          'callback' => 'formio_render_preview',
          'wrapper' => 'formio-form-render',
          'path' => 'system/formio_ajax',
          'method' => 'replace',
        ),
      ),
    ),
    'theme' => array(
      'form' => array(
        '#title' => t('Bootswatch Theme'),
        '#default_value' => '',
        '#states' => array(
          'invisible' => array(
            ':input[name="enable"]' => array('checked' => FALSE),
          ),
        ),
        '#ajax' => array(
          'callback' => 'formio_render_preview',
          'wrapper' => 'formio-form-render',
          'path' => 'system/formio_ajax',
          'method' => 'replace',
        ),
      ),
      'callback' => 'formio_check_bootswatch_api',
    ),
    'redirect' => array(
      'form' => array(
        '#title' => t('Redirect on submission'),
        '#description' => t('If the url is external include the schema. e.g., http://'),
        '#type' => 'textfield',
        '#default_value' => '',
      ),
      'validate' => 'formio_redirect_validate',
    ),
  );
}

/**
 * Get Bootswatch theme options from their api.
 *
 * @link https://bootswatch.com/
 *
 * @return array|bool
 *   Array of theme names if the api is up.
 */
function formio_bootswatch() {
  $swatches = array();
  $request = drupal_http_request('https://bootswatch.com/api/' . BOOTSWATCH_API . '.json');
  if ($request->code == 200) {
    $bootswatch = drupal_json_decode($request->data);

    foreach ($bootswatch['themes'] as $theme) {
      $name = $theme['name'];
      $key = strtolower($name);
      $swatches[$key] = $name;
    }
    return $swatches;
  }
  return FALSE;
}

/**
 * Callback that checks the bootswatch api.
 *
 * If the bootswatch api can't be reached for some reason we provide a text
 * field that allows entering the name of the theme. The user won't get a list
 * of options and will have to know the name of the theme but, this is better
 * than the form breaking.
 *
 * @param array $element
 *   The form element.
 */
function formio_check_bootswatch_api(&$element, $definition = NULL) {
  // Get possible Bootswatch themes from the Bootswatch api.
  $swatches = formio_bootswatch();
  // If we have trouble communicating with the api then just present the user
  // with a textfield so they can enter the name of the theme.
  if ($swatches == FALSE) {
    $element['#type'] = 'textfield';
  }
  // If we got a successful response from Bootswatch then populate a select
  // list with the options.
  else {
    $element['#type'] = 'select';
    $element['#options'] = $swatches;
  }
}

/**
 * Validation for the redirect URL.
 *
 * @param $form
 * @param $form_state
 */
function formio_redirect_validate(&$form, &$form_state) {
  // Check for a redirect url and ensure it contains a scheme if it is not
  // an internal path.
  if (!empty($form_state['values']['redirect'])) {
    $path = $form_state['values']['redirect'];
    $parsed = parse_url($path);
    $item = menu_get_item($path);
    // Set a form error.
    if (!$item && !isset($parsed['scheme'])) {
      form_set_error('redirect', t('You must include a scheme for external paths. e.g., "http(s)://"'));
    }
  }
}
