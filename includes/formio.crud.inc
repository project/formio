<?php
/**
 * @file
 * Ctools export crud operations.
 */

/**
 * Called when ctools_export_crud_delete() is called.
 *
 * The will override the default delete operation.
 *
 * @param object $item
 */
function formio_form_delete($item) {
  drupal_get_messages();
  // Delete blocks if they exist.
  $block = block_load('formio', 'formio__' . $item->drupal_name);
  if (isset($block->bid)) {
    if ($block->visibility == 0 && ($block->region == '-1' || !isset($block->region)) && empty($block->pages)) {
      db_delete('block')
        ->condition('bid', $block->bid)
        ->execute();
    }
    else {
      drupal_set_message(t("This block for this export is being used, you can't delete it."), 'error');
      return;
    }
  }

  if (db_table_exists('panels_pane')) {
    // Delete custom content types if they exist.
    $panes = db_select('panels_pane', 'pp')
      ->fields('pp', array('did'))
      ->condition('subtype', $item->drupal_name)
      ->execute()
      ->fetchCol();

    // @todo: Bad error. Look into this.
    if (!empty($panes)) {
      drupal_set_message(t("The export can't be deleted, a pane is using it.."), 'error');
      return;
    }
    foreach ($panes as $pane) {
      panels_delete_display($pane);
    }
  }

  // Delete the export.
  $schema = ctools_export_get_schema('formio_form');
  $form = $schema['export'];
  // If we were sent an object, get the export key from it. Otherwise
  // assume we were sent the export key.
  $value = is_object($item) ? $item->{$form['key']} : $item;
  db_delete('formio_form')
    ->condition($form['key'], $value)
    ->execute();
}
