<?php
/**
 * @file
 * Preprocessors and theme related functions.
 */

/**
 * Implements hook_preprocess_formio_preview().
 */
function formio_preprocess_formio_preview(&$vars) {
  if ($vars) {

  }
}
/**
 * Implements hook_preprocess_formio_form().
 */
function formio_preprocess_formio_form(&$vars) {
  $form = $vars['form'];
  if (isset($form) && is_array($form) && !empty($form)) {
    $vars['name'] = $form['name'];
    $vars['drupal_name'] = $form['drupal_name'];
    $vars['_id'] = $form['_id'];
    $vars['settings'] = $form['settings'];
  }
  if (!isset($vars['_id'])) {
    return;
  }

  // Path to the formio module.
  $vars['module_path'] = drupal_get_path('module', 'formio');

  // Get the project hash from the uri.
  $project = parse_url(trim(variable_get('formio_project_url', NULL)));
  list($vars['project_hash']) = explode('.', $project['host']);

  // The Bootswatch theme, if there is one.
  $vars['theme'] = (isset($vars['settings']['enable']) && !empty($vars['settings']['enable'])) ? '&theme=' . $vars['settings']['theme'] : '';

  // Authentication token for security.
  $token = restful_csrf_session_token();
  $vars['token'] = array_shift($token);

  // If a preset export is being created then we won't have a drupal_name yet.
  $drupal_name = !empty($vars['drupal_name']) ? $vars['drupal_name'] : 'new-preset';

  // The 'action' parameter of the Form.io form.
  $url = variable_get('restful_hook_menu_base_path', 'api') . '/v' . FORMIO_API_MAJOR . '.' . FORMIO_API_MINOR . '/' . $drupal_name;
  $vars['callback'] = isset($url) ? url($url, array('absolute' => TRUE)) : '';

}
