<?php
/**
 * @file
 * Form builder for elements used throughout the module.
 */

/**
 * Provides an edit form that both export_ui and content_type uses.
 *
 * @param $form
 * @param $form_state
 */
function formio_shared_edit_form(&$form, $form_state) {

  $item = $form_state['item'];

  // Start with the base form.
  formio_form_base($form, $form_state);

  // Include the Title and drupal_name elements.
//  formio_form_admin_info($form, $form_state);

  // Include the formio select list of forms.
  formio_select_list($form, $form_state);

  // Add custom elements to the form.
  formio_extra_settings_form($form, $form_state);

  ctools_include('dependent');
  ctools_add_js('dependent');

  // Include the Title and drupal_name elements.
  formio_form_admin_info($form, $form_state);

  // Provide a list of actions to perform. Leaving this but for now it just
  // returns a hidden form element set to 'entity'.
  formio_form_actions($form, $form_state);

  // The category used for the content type.
  $form['formio']['category'] = array(
    '#type' => 'textfield',
    '#title' => t('Category'),
    '#description' => t('What category this content should appear in. If left blank the category will be "Form.io".'),
    '#default_value' => isset($item->settings['category']) ? $item->settings['category'] : t('Form.io'),
    '#dependency' => array('edit-action' => array('reusable')),
  );

  // A description used for the content type.
  $form['formio']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Administrative description'),
    '#description' => t('A description of what this content is, does or is for, for administrative use.'),
    '#default_value' => isset($item->settings['description']) ? $item->settings['description'] : t('A Form.io form.'),
    '#dependency' => array('edit-action' => array('reusable')),
  );

  // Dont' allow the machine name to change.
  if ($form_state['op'] === 'edit') {
    $form['formio']['drupal_name']['#disabled'] = TRUE;
  }
}

/**
 * The base form that collects information used to create exports.
 *
 * This form should contain elements that exist across all forms being used to
 * collect information that is used to store a preset, display a Form.io form
 * in a panels pane or a block.
 *
 * @param array $form
 *   The current form.
 * @param array $form_state
 *   The current state of the form.
 * @throws \Exception
 */
function formio_form_base(&$form, $form_state) {
  // Add required libraries.
  drupal_add_library('formio', 'seamless');

  // The object that will be exported.
  $item = $form_state['item'];

  // Build the form.
  $form['formio'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preset'),
    '#attached' => array(
      'drupal_add_http_header' => formio_add_headers(),
      'library' => array('formio', 'seamless'),
    ),
  );

  // Only show a 'Preview' button when editing a form.
  if ($form_state['op'] === 'edit' && isset($item->_id)) {
    // Create the preview refresh button
    $form['preview_button'] = formio_form_preview_button();
  }

  // Add the container that will display the preview.
  formio_preview_container($form, $form_state);

}

/**
 * Adds markup to a form to render a preview.
 *
 * @param array $form
 *   The current form.
 * @param array $form_state
 *   The current state of the form.
 */
function formio_preview_container(&$form, $form_state) {
  // Placeholder for rendered formio forms.
  $form['formio_render'] = array(
    '#type' => 'fieldset',
    '#title' => t('Preview'),
    '#weight' => 999,
  );
  $id = drupal_html_id('formio-form-render');
  $form['formio_render']['formio_display'] = array(
    '#markup' => '<div id="' . $id . '"></div>',
    '#weight' => 0,
  );

  // @todo: Uses different base template. Testing.
  /*$image = ctools_image_path('favicon.ico', 'formio');
  $content = array(
    'title' => t('Formio'),
    'icon' => ctools_image_path('favicon.ico', 'formio'),
  );
  $form['formio_render']['formio_display'] = array(
    '#markup' => theme('formio_preview', $content),
    '#weight' => 999 ,
  );*/
}

/**
 * Adds title and machine name fields to the base form.
 *
 * @param array $form
 *   The current form.
 * @param array $form_state
 *   The current state of the form.
 */
function formio_form_admin_info(&$form, $form_state) {

  // The object that will be exported.
  $item = $form_state['item'];

  // This is set if this function is called from the export_ui. We will add
  // our own fields for this data.
  if (isset($form['info'])) {
    unset($form['info']);
  }

  // The title that will be used across the site administratively. This will
  // generate a drupal_name as well.
  $form['formio']['drupal_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('This will be used administratively as the title of the form.'),
    '#required' => TRUE,
    '#default_value' => isset($item->drupal_title) ? $item->drupal_title : '',
    '#size' => 40,
  );
  // The machine name.
  $form['formio']['drupal_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Drupal name'),
    '#machine_name' => array(
      'exists' => 'formio_form_name_exists',
      'source' => array('formio_form', 'drupal_title'),
    ),
    '#description' => t('The machine readable name of this form. It must be unique, and it must contain only alphanumeric characters and underscores. Once created, you will not be able to change this value!'),
    '#default_value' => isset($item->drupal_name) ? $item->drupal_name : '',
  );
}

/**
 * The preview button.
 *
 * @param string $class
 *   A comma separated list of class names to add.
 * @param int $weight
 *   The button's weight.
 *
 * @return array
 *   A renderable array that represents the preview button.
 */
function formio_form_preview_button($class = '', $weight = 0) {
  return array(
    '#type' => 'button',
    '#name' => 'preview',
    '#value' => t('Show preview'),
    '#attributes' => array('class' => array('formio-preview-button', $class)),
    '#ajax' => array(
      'callback' => 'formio_render_preview',
      'wrapper' => 'formio-form-widget-preview',
      'path' => 'system/formio_ajax',
      'method' => 'replace',
    ),
    '#weight' => $weight,
  );
}

/**
 * Builds a select list element used to select Form.io forms.
 *
 * Not adding this to the base form in case we want to change this widget.
 *
 * @return array
 *   A list of forms.
 */
function formio_select_list(&$form, $form_state) {

  // The object that will be exported.
  $item = $form_state['item'];

  // Load all the forms from Form.io under the current project.
  $formio_forms = formio_load_formio_forms();

  $options = array();

  if (!empty($formio_forms)) {
    // Add the Form.io forms as options in this form.
    $options = array();
    $options['_none'] = t('Select form');
    foreach ($formio_forms as $formio_form) {
      $options[$formio_form['_id']] = $formio_form['title'];
    }
  }

  $disabled = _formio_form_disable_select_list($form_state);

  // Form.io form selector.
  $id = drupal_html_id('formio-preset-form');
  $form['formio']['_id'] = array(
    '#title' => t('Form'),
    '#type' => 'select',
    '#limit_validation_errors' => array(),
    '#needs_validation' => FALSE,
    '#options' => $options,
    '#default_value' => isset($item->_id) ? $item->_id : '',
    '#required' => TRUE,
    '#prefix' => '<div id="' . $id . '">',
    '#suffix' => '</div>',
    '#disabled' => $disabled,
    '#ajax' => array(
      'callback' => 'formio_render_preview',
      'wrapper' => 'formio-form-render',
      'path' => 'system/formio_ajax',
      'method' => 'replace',
    ),
  );
}

/**
 * Determines if the select list is disabled.
 *
 * This element should render disabled if the Form.io form selected should not
 * be changed.
 *
 * @param array $form_state
 *   The current state of the form.
 *
 * @return bool
 */
function _formio_form_disable_select_list($form_state) {

  // The object that will be exported.
  $item = $form_state['item'];

  // The only option right now.
  if ($item->action === 'entity') {
    // Looking for components that have already been stored as fields.
    if (isset($item->components)) {
      // Check for component fields that may already contain data.
      foreach ($item->components as $component) {
        // Ignore buttons.
        if ($component['type'] === 'button') {
          continue;
        }
        // If any of the fields contain data disable the selector so the field
        // can't be changed.
        $field_name = 'formio_' . $component['key'];
        $info = field_info_field($field_name);
        if (!empty($info) && formio_bundle_field_has_data($info, $item->drupal_name)) {
          // Disable the select list so the Form.io form can't be changed.
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}

/**
 * Adds custom form elements to the base form.
 *
 * A module can create an "info" hook to add elements and callbacks on the form.
 *
 * @see formio_get_extra_settings().
 *
 * @param array $form
 *   The current form.
 * @param array $form_state
 *   The current state of the form.
 *
 * @return array
 */
function formio_extra_settings_form(&$form, &$form_state) {
  // Allow modules to add additional form elements to be used as settings
  // for the formio export form. For example, we add the ability to choose
  // a bootswatch theme and a field that allows a form redirect once the
  // Form.io form is submitted.
  $customizations = formio_get_extra_settings();

  // This will store this file in the form_state.
  ctools_form_include($form_state, 'formio_form', 'formio', 'content_types/formio_form');

  // The object that will be exported.
  $item = $form_state['item'];

  foreach ($customizations as $name => $customization) {
    $form['formio'][$name] = $customization['form'];
    // Set the default values.
    if (isset($item->settings[$name])) {
      $form['formio'][$name]['#default_value'] = $item->settings[$name];
    }
    // Check for a callback. A module may need to perform additional
    // operations such as checking the validity of an api call.
    // @see formio_formio_export_form_additional_settings().
    if (isset($customization['callback'])) {
      $function = $customization['callback'];
      if (function_exists($function)) {
        // Call the callback and pass the form element for alteration.
        $function($form['formio'][$name]);
      }
    }
    // Allow the module to also include validation and submission callbacks.
    if (!empty($customization['validate'])) {
      $form['#validate'][] = $customization['validate'];
    }
    if (!empty($customization['submit'])) {
      $form['#submit'][] = $customization['submit'];
    }
  }
}

/**
 * Set values that need to be stored in the form.
 *
 * @param array $form
 *   The current form.
 * @param array $form_state
 *   The current state of the form.
 */
function formio_set_values(&$form, $form_state) {

  // The object that will be exported.
  $item = $form_state['item'];

  // Load the form that was selected.
  $formio = isset($item->_id) ? formio_get_form($item->_id) : NULL;

  // Extra elements that need values.
  foreach (array('title', 'name', 'path', 'type', 'components') as $key) {
    $form[$key] = array(
      '#type' => 'value',
      '#value' => isset($formio, $formio[$key]) ? $formio[$key] : NULL,
    );
  }

  // Set the serial id of the form. Used for database storage.
  $form['fid'] = array(
    '#type' => 'value',
    '#value' => isset($item->fid) ? $item->fid : NULL,
  );

  // Get the created timestamp from the formio form.
  $form['created'] = array(
    '#type' => 'value',
    '#value' => isset($formio['created']) ? (new DateTime($formio['created']))->getTimestamp() : 0,
  );

  // Get the modified (changed) timestamp from the formio form.
  $form['modified'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($formio['modified']) ? (new DateTime($formio['modified']))->getTimestamp() : 0,
  );
}

/**
 * Adds actions to the base form.
 *
 * @param array $form
 *   The current form.
 * @param array $form_state
 *   The current state of the form.
 */
function formio_form_actions(&$form, $form_state) {
  // We only create entities as of now.
  $form['formio']['action'] = array(
    '#type' => 'hidden',
    '#value' => 'entity',
  );
  $form['formio']['action_info'] = array(
    '#type' =>'markup',
    '#value' => t('Form submissions will be stored as an entity. If you have 
enabled the <em>Save Submission</em> action at Form.io then your submission 
will be save there as well.'),
  );

  // @TODO: Give modules a chance to alter or add 'action' elements.
  drupal_alter('formio_actions', $form, $form_state);
}

/**
 * Wrapper for form validation.
 *
 * @param array $form
 *   The current form.
 * @param array $form_state
 *   The current state of the form.
 */
function formio_form_validate($form, $form_state) {
  if ($form_state['values']['_id'] === '_none') {
    form_set_error('_id', t('Looks like you forgot to select a form.'));
  }
}

/**
 * Use our own ajax callback for forms.
 *
 * We use this instead of what 'system/ajax' returns so we can customize it.
 *
 * @see ajax_form_callback().
 *
 * @return array|mixed
 */
function formio_ajax_form_callback() {
  list($form, $form_state, $form_id, $form_build_id, $commands) = ajax_get_form();

  // Process the form so we get form_state values.
  drupal_process_form($form_id, $form, $form_state);

  if (!$form_state['submitted']) {
    $callback = 'formio_render_preview';
  }
  if (!empty($form_state['triggering_element'])) {
    $callback = $form_state['triggering_element']['#ajax']['callback'];
  }
  if (!empty($callback) && function_exists($callback)) {
    form_state_values_clean($form_state);
    $result = $callback($form, $form_state);

    if (!(is_array($result) && isset($result['#type']) && $result['#type'] == 'ajax')) {
      // Turn the response into a #type=ajax array if it isn't one already.
      $result = array(
        '#type' => 'ajax',
        '#commands' => ajax_prepare_response($result),
      );
    }
    $result['#commands'] = array_merge($commands, $result['#commands']);

    return $result;
  }

  return array();
}

/**
 * Ajax callback to render a preview of the form.
 *
 * Renders the Form.io preview on the preset form.
 *
 * @param array $form
 *   Form that triggered the callback.
 * @param array $form_state
 *   State of the form.
 *
 * @return array
 *   Ajax commands.
 */
function formio_render_preview($form, $form_state) {
  // To render a preview we only need the Form.io _id and any values from the
  // form_state that might manipulate the way the form is displayed.

  // This radically simplifies the 'preview' of the form.
  $vars = &$form_state['values'];
  // Get custom settings for the render.
  $vars['settings'] = formio_filter_values($vars);

  if (isset($vars['_id'])) {
    // This gets the form from Form.io.
    $formio = (array) formio_get_form($vars['_id']);
    $content = array_merge($formio, $vars);

    $formio = theme('formio_form', $content);
    $commands[] = ajax_command_html('#formio-form-render', $formio);

    // Return commands to the system to be executed.
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  else {
    drupal_set_message(t('There was an error rendering the preview.'), 'error');
  }
}
