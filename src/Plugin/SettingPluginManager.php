<?php

namespace Drupal\formio;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\Factory\ContainerFactory;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\plug\Util\Module;

/**
 * Setting plugin manager.
 */
class SettingPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  protected $defaults = array(
    'label' => '',
    'type' => '',
    'ajax' => FALSE,
    'class' => 'Drupal\formio\Plugin\setting\Setting',
    'id' => '',
  );

  /**
   * Constructs the SettingPluginManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \DrupalCacheInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(\Traversable $namespaces, \DrupalCacheInterface $cache_backend) {
    parent::__construct(FALSE, $namespaces);
    $this->discovery = new YamlDiscovery('settings', Module::getDirectories());
    $this->factory = new ContainerFactory($this);
    // Allows invoking an alter hook. e.g., hook_formio_settings_plugin_alter().
    $this->alterInfo('formio_setting_plugin');
    $this->setCacheBackend($cache_backend, 'formio_setting_plugins');
  }

  /**
   * SettingPluginManager factory method.
   *
   * @param string $bin
   *   The cache bin for the plugin manager.
   *
   * @return SettingPluginManager
   *   The created manager.
   */
  public static function create($bin = 'cache') {
    return new static(Module::getNamespaces(), _cache_get_object($bin));
  }

}
