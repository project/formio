<?php
namespace Drupal\formio\Plugin\setting;

interface SettingInterface {

  /**
   * Builds a setting field.
   *
   * @return array
   *   The setting.
   */
  public function build();

}
