<?php

namespace Drupal\formio\Plugin\setting;

use Drupal\Component\Plugin\PluginBase;

class Setting extends PluginBase implements SettingInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $definition = $this->getPluginDefinition();
    $element = array(
      '#title' => $definition['label'],
      '#type' => $definition['type'],
    );
    if (!empty($definition['states'])) {
      $states = array(
        $definition['states']['action'] => array(
          ':' . $definition['states']['type'] . '[name="' . $definition['states']['target'] . '"]' => array(
            $definition['states']['condition']['key'] => $definition['states']['condition']['value'],
          ),
        ),
      );
      $element['#states'] = $states;
    }
    if (!empty($definition['ajax'])) {
      $ajax = array(
        'callback' => 'formio_render_preview',
        'wrapper' => 'formio-form-render',
        'path' => 'system/formio_ajax',
        'method' => 'replace',
      );
      $element['#ajax'] = $ajax;
    }
    if (isset($definition['callback']) && function_exists($definition['callback'])) {
      $definition['callback']($element, $definition);
    }
    if (isset($definition['validate'])) {
      $element['#validate'] = $definition['validate'];
    }

    return $element;
  }

}
