<?php

namespace Drupal\formio\Plugin\resource\DataProvider;

use Drupal\restful\Plugin\resource\DataProvider\DataProviderEntity;
use Drupal\restful\Plugin\resource\DataProvider\DataProviderInterface;

class DataProviderFormio extends DataProviderEntity implements DataProviderInterface {

  public function entityPreSave(\EntityDrupalWrapper $wrapper) {
    $formio = $wrapper->value();

    ctools_include('action', 'formio');
    $result = formio_submission_handler('formio_form', $formio->instance);

    if (!empty($result['error'])) {
      $output = $result['error'];

      // Encode the output.
      drupal_json_output(json_encode($output));

      // Exit.
      drupal_exit();

    }

    if (!empty($formio->sid) || !empty($result['error'])) {
      return;
    }
    // Load the export.
    $form = formio_load_drupal_form($formio->instance);

    $formio->_id = $form->_id;
    $formio->name = $form->name;
    $formio->created = $formio->changed = REQUEST_TIME;
    $formio->owner = $this->getAccount()->uid;
  }

  public function update($identifier, $object, $replace = FALSE) {
    return parent::update($identifier, $object, $replace);
  }
}
