<?php

/**
 * Interface FormioActionsInterface
 */
interface FormioActionsInterface {

  /**
   * Used to implement the action.
   *
   * @param array $plugin
   * @param string $preset_name
   * @param object $form
   */
  public function action($plugin, $preset_name, $form);
}

/**
 * Class FormioActionsManager
 */
abstract class FormioActionsManager implements FormioActionsInterface {

  /**
   * @var object
   *   The plugin.
   */
  var $plugin;

  /**
   * @var string
   *   The name of the plugin.
   */
  var $name;

  /**
   * @var string
   *   A human readable name of the plugin.
   */
  var $title;

  /**
   * Initialize the plugin.
   *
   * @param object $plugin
   */
  public function init($plugin) {
    $this->plugin = $plugin;
    $this->name = $plugin['name'];
    $this->title = isset($plugin['title']) ? $plugin['title'] : ucwords(str_replace('_', ' ', $plugin['name']));
  }

  /**
   * Used to define a menu item used by the form for a callback.
   *
   * @param array $items
   *   Menu items.
   */
  public function hook_menu(&$items) {
    foreach ($items as &$item) {
      // Add menu item defaults.
      $item += array(
        'file' => 'action.inc',
        'file path' => drupal_get_path('module', 'formio') . '/includes',
      );
    }
  }

}
