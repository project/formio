<?php
/**
 * @file
 * Action plugin to define the default action when a form is submitted.
 */

$plugin = array(
  'title' => t('Default action'),
  'handler' => array(
    'class' => 'FormioDefaultAction',
  ),
  'has menu' => TRUE,
  'has actions' => TRUE,
  'access' => TRUE,

);
