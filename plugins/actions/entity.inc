<?php
/**
 * @file
 * Action plugin to create an entity on form submission.
 */

$plugin = array(
  'title' => t('Create Entity'),

  'handler' => array(
    'class' => 'FormioEntityAction',
    'parent' => 'FormioDefaultAction',
  ),

  'has menu' => FALSE,
  'has actions' => TRUE,
  'access' => TRUE,

);
