<?php
/**
 * @file
 * Contains the FormioDualAction class.
 */

/**
 * Class FormioDualAction
 */
class FormioDualAction extends FormioDefaultAction {

  /**
   * {@inheritdoc}
   */
  public function action($plugin, $preset_name, $form) {
    $action =
    parent::action($plugin, $preset_name, $form);
  }

  /**
   * Makes a restful call to Form.io.
   *
   * @param string $token
   *   The x-token used for authentication.
   * @param string $uri
   *   The api uri.
   * @param string $data
   *   A json object containing the data submitted in the form.
   *
   * @return array
   *   The result of the rest call.
   */
  private function rest_call($token, $uri, $data) {
    // Initialize the session.
    $curl = curl_init();

    // Setup options for the transfer.
    curl_setopt_array($curl, array(
      // Adding a query here for a 'Dry Run'.
      CURLOPT_URL => $uri, // . '?dryrun=1',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "$data",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json",
        "x-token: " . $token,
      ),
    ));

    // Execute the session.
    $response = curl_exec($curl);

    // The last error, if any, for the current session.
    $error = curl_error($curl);

    // The session result containing the response and any errors.
    $result = array(
      'response' => $response,
      'error' => $error,
    );

    // Close the session.
    curl_close($curl);

    return $result;
  }
}
