<?php
/**
 * @file
 * Action plugin creates entity and sends submission to Form.io.
 */

$plugin = array(
  'title' => t('Create Entity and Send to Form.io'),
  'handler' => array(
    'class' => 'FormioDualAction',
    'parent' => 'FormioDefaultAction',
  ),
  'has menu' => FALSE,
  'has actions' => TRUE,
  'access' => TRUE,

);
