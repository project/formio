<?php
/**
 * @file
 * Contains the FormioDefault class.
 */

/**
 * Class FormioDefaultAction
 */
class FormioDefaultAction extends FormioActionsManager {

  /**
   * {@inheritdoc}
   */
  public function hook_menu(&$items) {
    $items['formio/%'] = array(
      'page callback' => $this->plugin['callback'],
      'page arguments' => array($this->plugin['name'], 1),
      'access callback' => $this->plugin['access'],
      'type' => MENU_CALLBACK,
    );
    // Provides some defaults.
    parent::hook_menu($items);
  }

  /**
   * {@inheritdoc}
   */
  public function action($plugin, $preset_name, $form) {

    // A json object containing the contents of the form submission.
    $data = file_get_contents('php://input');

    $request = formio_rest('form/' . $form->_id);
    $results = $request->fetchKeyed('path');

    // The name given by Form.io.
    $name = $form['name'];
    // The form we are targeting.
    $target = $results[$name];

    // We should always have a path but in case we don't throw an error.
    $path = $target['path'];
    if (!isset($path)) {
      // @todo: Catch errors.
      drupal_json_output(json_encode('Example: Could not find a path'));

      drupal_exit();
    }

    // Get the project url.
    $project = variable_get('formio_project_url', FALSE);

    // The api uri for a submission.
    $uri = $project . '/'. $path . '/submission';

    // This is used to validate the request. This will be posted back to form.io
    // and the result will need to be a 200. Add a ?dryrun=1 query to the post.
    $token = variable_get('formio_project_api_key', FALSE);
    // $token = getallheaders()['X-Token'];

    // Make a rest call to Form.io and return it.
    $call = $this->rest_call($token, $uri, $data);
    return $call;
  }

  /**
   * Makes a restful call to Form.io.
   *
   * @param string $token
   *   The x-token used for authentication.
   * @param string $uri
   *   The api uri.
   * @param string $data
   *   A json object containing the data submitted in the form.
   *
   * @return array
   *   The result of the rest call.
   */
  private function rest_call($token, $uri, $data) {
    // Initialize the session.
    $curl = curl_init();

    // Setup options for the transfer.
    curl_setopt_array($curl, array(
      // Adding a query here for a 'Dry Run'.
      CURLOPT_URL => $uri, // . '?dryrun=1',
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "$data",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json",
        "x-token: " . $token,
      ),
    ));

    // Execute the session.
    $response = curl_exec($curl);

    // The last error, if any, for the current session.
    $error = curl_error($curl);

    // The session result containing the response and any errors.
    $result = array(
      'response' => $response,
      'error' => $error,
    );

    // Close the session.
    curl_close($curl);

    return $result;
  }
}
