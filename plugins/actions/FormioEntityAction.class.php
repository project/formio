<?php
/**
 * @file
 * Defines the FormioEntity class.
 */

/**
 * Class FormioEntityAction
 */
class FormioEntityAction extends FormioDefaultAction {

  /**
   * The base action for creating an entity.
   *
   * @param array $plugin
   * @param string $preset_name
   * @param object $form
   */
  public function action($plugin, $preset_name, $form) {
    $formio = formio_rest('form/' . $form->id);
    $settings = $form->settings;
    $entity_type = $settings['entity_type'];
    $bundle = $settings['entity_bundle'];

    // The form submission data.
    $data = json_decode(file_get_contents('php://input'));

    // Check for a plugin that deals specifically with this entity type.
    list($module) = explode(':', $form->action);
    // Look for a plugin that might want to handle creating this entity type
    // on a more granular level.
    $plugin = ctools_get_plugins($module, 'actions', $entity_type);
    if (!empty($plugin['action'])) {
      $function = $plugin['action'];
      if (function_exists($function)) {
        $success = $function($formio, $data, $form, $bundle, $settings['entity_field_map']);
        if ($success) {
          parent::action($plugin, $preset_name, $form);
        }
      }
    }
    // If we don't find one we'll just do the best we can.
    else {
      $entity = entity_create($entity_type, array('type' => $bundle));
      $entity_wrapper = entity_metadata_wrapper($entity_type, $entity);
      foreach ($settings['entity_field_map'] as $drupal_field => $formio_field) {
        if ($drupal_field == 'title') {
          $entity_wrapper->title = $data->data->{$formio_field};
        }
        else {
          $instance = field_info_instance($entity_type, $drupal_field, $bundle);
          if ($instance) {
            $entity_wrapper->{$instance['field_name']} = array('value' => $data->data->{$formio_field});
          }
        }
      }
      // Save the entity
      if (isset($entity_wrapper)) {
        $entity_wrapper->save();
        parent::action($plugin, $preset_name, $form);
      }
    }
  }
}
