<?php
/**
 * @file
 * A ctools content type plugin that renders a form.io form.
 */

$plugin = array(
  'title' => t('Custom Form.io'),
//  'no title override' => TRUE,
  'icon' => ctools_image_path('favicon.ico', 'formio'),
  'description' => t('Renders an embedded form from Form.io'),
  'bootswatch api' => 'formio_custom_form_content_type_bootswatch',
  'defaults' => array(
    'drupal_title' => '',
    'drupal_name' => '',
    '_id' => '',
    'title' => '',
    'name' => '',
    'path' => '',
    'type' => '',
    'components' => '',
    'created' => '',
    'modified' => '',
    'action' => '_none',
  ),
  // Make sure the edit form is only used for some subtypes.
  'edit form' => '',
  'add form' => '',
  'edit text' => t('Plugin edit'),
  'all contexts' => TRUE,
);

/**
 * Return forms that have this subtype id.
 *
 * @param string $subtype_id
 *   The id of the subtype.
 *
 * @return
 */
function formio_custom_form_content_type_content_type($subtype_id) {
  if ($subtype_id == 'custom_form') {
    return _formio_default_content_type_content_type();
  }
  else if ($form = formio_load_drupal_form($subtype_id)) {
    return _formio_custom_form_content_type_content_type($form);
  }
}

/**
 * Returns all of the available custom formio content types available.
 *
 * @return array
 *   Content types that render a form for each export.
 */
function formio_custom_form_content_type_content_types() {
  $content_types = &drupal_static(__FUNCTION__);
  if (isset($content_types)) {
    return $content_types;
  }

  $content_types = array();
  $content_types['custom_form'] = _formio_default_content_type_content_type();

  $forms = formio_load_drupal_forms();
  foreach ($forms as $drupal_name => $form) {
    $content_types[$drupal_name] = _formio_custom_form_content_type_content_type($form);
  }

  return $content_types;
}

/**
 * Custom settings for the default formio custom content type.
 *
 * This is the original content type that allows creating new types.
 */
function _formio_default_content_type_content_type() {
  $now = time();
  $category = t('Form.io');
  return array(
    'drupal_title' => t('Form.io form title'),
    'drupal_name' => 'custom_form',
    '_id' => NULL,
    'title' => t('New Form.io form'),
    'path' => t('Form.io form path'),
    'type' => t('Form.io form type'),
    'components' => array(),
    'created' => $now,
    'changed' => $now,
    'action' => '_none',
    // We don't have columns for these values so we store them in the
    // serialized array.
    'settings' => array(
      'description' => t('Customize a new Form.io form.'),
      'category' => $category,
    ),
    // Only setting this so that:
    // panels_renderer_editor.class.php->get_category() sets this.
    'category' => $category,
    'top level' => TRUE,
    'edit form' => 'formio_custom_form_content_type_edit_form',
    'add form' => 'formio_custom_form_content_type_edit_form',
    'all contexts' => TRUE,
    'check editable' => 'formio_custom_form_content_type_editable',
  );
}

/**
 * Returns an info array for a specific formio custom content type.
 *
 * @param object $content
 *   The export loaded from the database.
 *
 * @return array
 */
function _formio_custom_form_content_type_content_type($content) {
  $customizations = array_keys(formio_get_extra_settings());
  $category = isset($content->settings['category']) ? check_plain($content->settings['category']) : t('Form.io');
  $type = array(
    'drupal_title' => check_plain($content->drupal_title),
    'drupal_name' => $content->drupal_name,
    '_id' => $content->_id,
    'title' => $content->drupal_title,
    'name' => $content->name,
    'path' => $content->path,
    'type' => $content->type,
    'components' => $content->components,
    'created' => $content->created,
    'changed' => $content->changed,
    'action' => $content->action,
    // We don't have columns for these values so we store them in the
    // serialized array.
    'settings' => array(
      'description' => isset($content->settings['description']) ? check_plain($content->settings['description']) : '',
      'category' => $category,
    ),
    // Only setting this so that:
    // panels_renderer_editor.class.php->get_category() sets this.
    'category' => $category,
    'all contexts' => TRUE,
    'icon' => ctools_image_path('favicon.ico', 'formio'),
    // Makes it easier to access the form.
    'content' => $content,
  );

  // A module can add their own custom settings so we include them here.
  $settings = array();
  foreach ($customizations as $key) {
    $settings[$key] = $content->settings[$key];
  }

  $settings = array_merge($type['settings'], $settings);
  $type['settings'] = $settings;

  return $type;
}

/**
 * Given a subtype and a $conf, return the actual settings to use.
 *
 * The actual settings may be stored directly in the pane or this may
 * be a pointer to re-usable content that may be in the database or in
 * an export. We have to determine from the subtype whether or not it
 * is local or shared custom content.
 *
 * @param array $subtype
 *   The subtype of this content type.
 * @param array $conf
 *   The configurations stored for this content type. The values stored in form
 *   fields and from the Form.io form.
 *
 * @return array
 *
 * @todo this is almost working....keep following custom.inc to see how ctools is doing it
 *
 */
function _formio_custom_form_content_type_get_conf($subtype, $conf) {
  $settings = array();
  if (isset($subtype) && $subtype['drupal_name'] !== 'custom_form') {
    $settings = $subtype['content']->settings;
    $settings['custom_type'] = 'fixed';
    $settings['content'] = $subtype['content'];
    $settings['drupal_title'] = $settings['content']->drupal_title;
    // A description won't exist if this export was created outside of panels.
    $settings['description'] = !empty($settings['content']->settings['description']) ? $settings['content']->settings['description'] : t('A Form.io Form');
  }
  // This means the form was created as custom content and it was set to
  // to be reusable.
  //
  // Since we're not allowed to change the subtype, we're still stored as
  // though we are local, but are pointing off to non-local.
  else if ($conf['action'] === 'reusable' && !empty($conf['drupal_name'])) {
    ctools_include('export');
    $content = ctools_export_crud_load('formio_form', $conf['drupal_name']);
    if ($content) {
      $settings = $content->settings;
      $settings['custom_type'] = 'fixed';
      $settings['content'] = $content;
      $settings['drupal_title'] = $content->drupal_title;
      // A description won't exist if this export was created outside of panels.
      $settings['description'] = !empty($settings['content']->settings['description']) ? $settings['content']->settings['description'] : 'Form.io Form';
    }
    else {
      $content = ctools_export_crud_new('formio_form');
      $content->drupal_name = $conf['drupal_name'];
      $now = time();
      $settings = array(
        'drupal_title' => t('Missing/deleted content'),
        '_id' => NULL,
        'title' => '',
        'drupal_name' => '',
        'name' => '',
        'path' => '',
        'type' => '',
        'components' => array(),
        'created' => $now,
        'changed' => $now,
        'custom_type' => 'fixed',
        'action' => '_none',
        'content' => $content,
      );
      $customizations = formio_get_extra_settings();
    }
  }
  else if (!isset($subtype)) {
    // The export was deleted.
    drupal_set_message(t('BAD'), 'error');
  }
  // This means that it is created as custom and has not been set to
  // reusable.
  else {
    $settings = $conf;
    $settings['custom_type'] = 'local';
  }

  return $settings;
}

/**
 * Implements the plugin edit form.
 *
 * This is the edit form for the content type. It's used to collect additional
 * information apart from the export.
 */
function formio_custom_form_content_type_edit_form($form, &$form_state) {

  // This will store this file in the form_state.
  formio_form_include_plugin($form_state, 'custom_form', 'content_types');

  // Include our form file.
  ctools_include('formio.form', 'formio');

  // Get settings for the edit form.
  $settings = _formio_custom_form_content_type_get_conf($form_state['subtype'], $form_state['conf']);
  $form_state['settings'] = $settings;

  // We don't need a form.
  if ($settings['custom_type'] == 'fixed') {
    return $form;
  }

  formio_shared_edit_form($form, $form_state);

  // Let modules modify the content type form.
  drupal_alter('formio_custom_form_content_type_form', $form);

  return $form;
}

function formio_custom_form_content_type_edit_form_validate($form, &$form_state) {
  formio_form_validate($form, $form_state);
  if ($form_state['settings']['custom_type'] != 'fixed' && $form_state['settings']['actions'] === 'reusable') {
    if (empty($form_state['values']['drupal_name'])) {
      form_error($form['drupal_name'], t('Name is required.'));
    }

    // Check for string identifier sanity
    if (!preg_match('!^[a-z0-9_]+$!', $form_state['values']['drupal_name'])) {
      form_error($form['drupal_name'], t('The name can only consist of lowercase letters, underscores, and numbers.'));
      return;
    }

    ctools_include('export');
    // Check for name collision
    if ($form_state['values']['drupal_name'] == 'formio_form' || (ctools_export_crud_load('formio_form', $form_state['values']['drupal_name']))) {
      form_error($form['drupal_name'], t('Content with this name already exists. Please choose another name or delete the existing item before creating a new one.'));
    }
  }
//  if ($form_state['values']['create_entity']) {
//    // Reorder submit functions so we call this file's first and the
//    // export_ui/formio_form submit function second.
//    $this_submit = array_shift($form['#submit']);
//    array_unshift($form['#submit'], $this_submit, 'formio_form_edit_form_submit');
//  }
}

/**
 * Implements the plugin edit form submission.
 * formio_custom_form_content_type_edit_form_submit
 */
function formio_custom_form_content_type_edit_form_submit($form, &$form_state) {

  $action = $form_state['values']['action'];

  // If this form isn't reusable then just store the settings.
  if ($form_state['settings']['custom_type'] == 'fixed') {
    _formio_custom_form_content_type_export_save($form_state['settings']['content'], $form_state);
  }
  // Create an export of the form if the 'reusable' checkbox is checked.
  elseif ($action === 'reusable') {

    // Create a new instance of the exportable object.
    $item = ctools_export_crud_new('formio_form');

    // Grab the form from Form.io so we can fill in values.
    $formio = formio_get_form($form_state['values']['_id']);

    // Combines the Form.io form values with custom values defined locally.
    formio_form_combine_values($formio, $item, $form_state, TRUE);

    // Save an export.
    _formio_custom_form_content_type_export_save($item, $form_state);
    $form_state['conf']['drupal_name'] = $item->drupal_name;

//    // Load the $plugin information
//    ctools_include('plugins');
//    $plugin = ctools_get_plugins('ctools', 'export_ui', 'formio_form');

  }
  elseif ($action === 'entity' || $action === 'both') {
    // The object that will be exported.
    $item = $form_state['item'];
    // The unique form _id from Form.io.
    $item->_id = $form_state['values']['_id'];
    // Get the form from Form.io.
    $formio = formio_get_form($item->_id);
    // The bundle for the entity.
    $drupal_name = !empty($item->drupal_name) ? $item->drupal_name : $form_state['values']['drupal_name'];

    // Build the data field component.
    $data = array(
      'label' => 'Data',
      'key' => 'data',
      'type' => 'serial'
    );

    // Add a data field to store raw json.
    formio_add_field($drupal_name, $data);

    // Retrieve all components from the form.
    $components = formio_flatten_components($formio['components']);
    foreach($components as $component) {
      // Add a new field for each of the components. Ignore 'buttons'.
      if ($component['input'] && $component['type'] !== 'button') {
        formio_add_field($drupal_name, $component);
      }
    }
  }
  else {
    // Save values into the conf.
    foreach (array_keys($form_state['plugin']['defaults']) as $key) {
      $form_state['conf'][$key] = isset($form_state['values'][$key]) ? $form_state['values'][$key] : $form_state['plugin']['defaults'][$key];
    }
    foreach (array_keys(formio_get_extra_settings()) as $key) {
      $form_state['conf'][$key] = isset($form_state['values'][$key]) ? $form_state['values'][$key] : '';
    }
  }
}

/**
 * @todo: Create a new entity if the checkbox is checked. Otherwise, just save the preset.
 *
 * @param $item
 * @param $form_state
 */
function _formio_custom_form_content_type_export_save($item, $form_state) {
  // Apply updates to the content object.
  $item->settings['category'] = $form_state['values']['category'];
  $item->settings['description'] = $form_state['values']['description'];

  // Save values into the conf.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $item->{$key} = isset($form_state['values'][$key]) ? $form_state['values'][$key] : $form_state['plugin']['defaults'][$key];
  }
  foreach (array_keys(formio_get_extra_settings()) as $key) {
    $item->settings[$key] = $form_state['values'][$key];
  }

  ctools_export_crud_save('formio_form', $item);
}

function formio_custom_form_content_type_editable($content_type, $subtype, $conf) {
  if ($subtype['drupal_name'] == 'custom_form' && !empty($conf['drupal_name'])) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Implements the plugin render callback.
 *
 * @param string $subtype
 *   This will be formio_form.
 * @param array $conf
 *   Will only contain data if this is being rendered via panels.
 * @param array $args
 *   Extra arguments. These can come from panels but are primarily used for
 *   rendering a block.
 * @param object $context
 *   Not really used at this point but could require that a specific context
 *   be present.
 *
 * @return array
 *   A render array.
 */
function formio_custom_form_content_type_render($subtype, $conf, $args, $context, $incoming_content) {
  $settings = _formio_custom_form_content_type_get_conf(formio_custom_form_content_type_content_type($subtype), $conf);

  // NEW...

  static $delta = 0;

  $block = new stdClass();
  $block->subtype = ++$delta;
  $block->title = filter_xss_admin($settings['drupal_title']);

  if ($settings['custom_type'] == 'fixed' && user_access('administer formio custom content')) {
    $block->admin_links = array(
      array(
        'title' => t('Configure'),
        'alt' => t("Configure this panels pane."),
        'href' => 'admin/structure/formio/list/' . $subtype . '/edit',
        'query' => drupal_get_destination(),
      ),
    );
    $formio = formio_load_drupal_form($subtype);
  }
  else if ($settings['custom_type'] == 'local' & user_access('submit forms to formio')) {
    // Load the formio form.
    $formio = formio_get_form($settings['_id']);
    // Add a settings array for customizations.
    $formio['settings'] = array();

    // Check for custom settings.
    $customs = array_keys(formio_get_extra_settings());
    foreach ($customs as $custom) {
      $formio['settings'][$custom] = $settings[$custom];
    }
  }

  if (!empty($formio)) {
    $content = array(
      '#theme' => 'formio_form',
      '#form' => (array) $formio,
      '#attached' => array(
        'drupal_add_http_header' => formio_add_headers(),
        // This adds seamless to the render.
        'library' => array(
          array('formio', 'seamless'),
        ),
      ),
    );
    $block->content = $content;
  }

  return $block;
}

/**
 * Implements ctools content types plugin hook theme.
 */
function formio_custom_form_content_type_theme(&$theme, $plugin) {
  $path = $plugin['path'] . '/theme';
  $theme['formio_form'] = array(
    'variables' => array(
      'form' => NULL,
    ),
    'path' => $path,
    'template' => 'formio-form',
  );
}

/**
 * Provides administrative information on the pane.
 *
 * @param $subtype
 * @param $conf
 * @return \stdClass
 */
function formio_custom_form_content_type_admin_info($subtype, $conf) {
  $settings = _formio_custom_form_content_type_get_conf(formio_custom_form_content_type_content_type($subtype), $conf);

  $block = new stdClass();
  $block->title = isset($settings['drupal_title']) ? filter_xss_admin($settings['drupal_title']) : '';
  $block->content = isset($settings['description']) ? filter_xss_admin($settings['description']) : 'No description';
  return $block;
}

/**
 * Provides administrative information on the pane.
 *
 * @param $subtype
 * @param $conf
 * @return null|string
 */
function formio_custom_form_content_type_admin_title($subtype, $conf) {
  $settings = _formio_custom_form_content_type_get_conf(formio_custom_form_content_type_content_type($subtype), $conf);

  $output = t('Form.io Form');
  if (empty($settings)) {
    $output = t('Missing/deleted content');
  }
  $title = !empty($settings['drupal_title']) ? $settings['drupal_title'] : NULL;
  if ($title) {
    if ($settings['custom_type'] != 'fixed') {
      $output = t('Form.io: @title', array('@title' => $title));
    }
    else {
      $output = $title;
    }
  }

  return $output;
}
