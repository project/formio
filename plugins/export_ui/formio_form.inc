<?php
/**
 * @file
 * Defines a ctools Export UI plugin for Form IO presets.
 *
 * This is what handles the actual preset form. The rendering of the form.io
 * forms is handled by the ctools content type.
 */

$plugin = array(
  // As defined in hook_schema().
  'schema' => 'formio_form',
  // Define a permission users must have to access these pages.
  'access' => 'administer formio',

  // Define the menu item.
  'menu' => array(
    // The 'menu prefix' and 'menu item' create the base path. The default path
    // is 'admin/structure/'. This will give us 'admin/structure/formio',
    // 'admin/structure/formio/add'... @see ctools_export_ui_plugin_base_path().
    'menu item' => 'formio',
    'menu title' => 'Form.io forms',
    'menu description' => 'Manage imported forms and where to store them.',
    'items' => array(),
  ),

  // Define user interface texts.
  'title singular' => t('Form.io form'),
  'title plural' => t('Form.io forms'),
  'title singular proper' => t('Form.io Form'),
  'title plural proper' => t('Form.io Forms'),

  'form' => array(
    'settings' => 'formio_form_edit_form',
    'validate' => 'formio_form_edit_form_validate',
    'submit' => 'formio_form_edit_form_submit',
  ),

  'add_form' => t('!message', array('!message' => l('Add a new form!', 'admin/structure/formio/add'))),
  'need_key' => t('Looks like you need an API Key. Enter one !here or visit !formio to create one now.', array('!here' => l('Here', 'admin/config/formio/settings'), '!formio' => l('Form.io', 'https://form.io'))),

  'strings' => array(
    'message' => array(),
  ),

  'handler' => array(
    'class' => 'formio_form_ui',
    'parent' => 'ctools_export_ui',
  ),
);

// Determines a message when there are no items to display.
$plugin['strings']['message']['no items'] = (!empty(formio_apikey())) ? $plugin['add_form'] : $plugin['need_key'];

/**
 * Renders the form when creating a preset.
 *
 * @param array $form
 * @param array $form_state
 * @return array
 */
function formio_form_display_form($form, &$form_state) {
  return $form;
}

/**
 * Implements formio_form_edit_form().
 *
 * This is the preset edit form for ctools exports.
 */
function formio_form_edit_form(&$form, &$form_state) {

  // This will store this file in the $form_state.
  formio_form_include_plugin($form_state, 'formio_form', 'export_ui');

  // Include our form file.
  ctools_include('formio.form', 'formio');

  formio_shared_edit_form($form, $form_state);

  // Let modules modify the export preset form.
  drupal_alter('formio_export_ui_form', $form);
}

/**
 * Implements formio_form_edit_form_validate().
 */
function formio_form_edit_form_validate(&$form, &$form_state) {
  formio_form_validate($form, $form_state);
}

/**
 * Implements formio_form_edit_form().
 */
function formio_form_edit_form_submit(&$form, &$form_state) {

  // The object that will be exported.
  $item = $form_state['item'];

  // The bundle for the entity.
  $drupal_name = !empty($item->drupal_name) ? $item->drupal_name : $form_state['values']['drupal_name'];

  // Get custom settings.
  $settings = formio_filter_values($form_state['values']);

  // This is used with panels.
  $settings['category'] = isset($form_state['values']['category']) ? $form_state['values']['category'] : t('Form.io');
  $settings['description'] = isset($form_state['values']['description']) ? $form_state['values']['description'] : t('A Form.io form.');

  // Move the custom settings to the appropriate array.
  foreach ($settings as $key => $setting) {
    $form_state['values']['settings'][$key] = $setting;
  }
  $item->settings = $form_state['values']['settings'];

  // Set some values.
  $item->action = $form_state['values']['action'];
  $item->drupal_title = $form_state['values']['drupal_title'];
  $item->drupal_name = $form_state['values']['drupal_name'];

  // This is used with panels.
  $item->settings['category'] = $settings['category'];
  $item->settings['description'] = $settings['description'];

  // The unique form _id from Form.io.
  $item->_id = $form_state['values']['_id'];

  // Get the form from Form.io.
  $formio = formio_get_form($item->_id);

  // Update the export object.
  foreach ($formio as $key => $value) {
    // Convert the modified and created values to timestamps.
    if ($key === 'modified' || $key === 'created') {
      $item->{$key} = (new DateTime($value))->getTimestamp();
    }
    // Everything else.
    else {
      $item->{$key} = $value;
    }
  }

  // Allow altering what is stored in the exported item.
  drupal_alter('formio_export_ui_item', $form_state, $item);

  // Build the data field component.
  $data = array(
    'label' => 'Data',
    'key' => 'data',
    'type' => 'serial'
  );

  // Add a data field to store raw json.
  formio_add_field($drupal_name, $data);

  // Retrieve all components from the form.
  $components = formio_flatten_components($formio['components']);
  foreach($components as $component) {
    // Add a new field for each of the components. Ignore 'buttons'.
    if ($component['input'] && $component['type'] !== 'button') {
      formio_add_field($drupal_name, $component);
    }
  }

  // Clear ctools object-cache.
  formio_clear_form_cache($drupal_name);

  // Flush all the caches so our endpoint is available.
  module_invoke_all('flush_caches');

  // @todo: Find out if and why this is necessary.
  module_invoke('system', 'cron');
}

/**
 * Ajax callback for ctools preset export form.
 */
function formio_preset_ajax_submit($form, &$form_state) {
  if (form_get_errors()) {
    $commands = array();
    $messages = theme('status_messages');
    $system_message = drupal_get_messages();
    // Remove the old one if it exists.
//    $commands[] = ajax_command_remove('.messages');
    // Add a new error to the page.
    $commands[] = ajax_command_prepend('#page', $messages);
    print ajax_render($commands);
    exit;
  }
  else {
    return $form;
  }
}

/**
 * Create a new field and field instance for each form component.
 *
 * @param string $bundle
 *   The entity bundle.
 * @param array $component
 *   The Formio form component.
 *
 * @return array
 *   A field instance.
 */
function formio_add_field($bundle, $component) {
  // The name of our entity.
  $entity = 'formio';

  // Convert from camelCase to underscore_notation and prefix with formio_.
  $key = formio_convert_formio_field_to_drupal_field($component['key']);
  // The type of component.
  $type = $component['type'];
  $label = $component['label'];

  // See if we have defined a field or field_instance for this component.
  $field = field_info_field($key);
  $instance = field_info_instance($entity, $key, $bundle);

  $field_type = '';
  $widget_type = '';
  $settings = array();

  switch ($type) {
    case 'textfield':
    case 'phoneNumber':
    case 'email':
    case 'password':
    case 'datetime':
    case 'select':
      $field_type = 'text';
      $widget_type = 'text_textfield';
      $settings = array();
      break;
    case 'textarea':
    case 'serial':
    case 'address':
    case 'file':
    case 'survey':
    case 'datagrid':
    case 'selectboxes':
    case 'currency':
    case 'checkbox':
    case 'radio':
      $field_type = 'text';
      $widget_type = 'text_textarea';
      $settings = array('rows' => 5);
      break;
    case 'number':
      $field_type = 'number_decimal';
      $widget_type = 'number';
      $settings = array (
        'min' => !empty($component['validate']['min']) ? $component['validate']['min'] : '',
        'max' => !empty($component['validate']['max']) ? $component['validate']['max'] : '',
        'prefix' => !empty($component['prefix']) ? $component['prefix'] : '',
        'suffix' => !empty($component['suffix']) ? $component['suffix'] : '',
      );
      break;
  }

  // Create the field and instance.
  if (empty($field)) {
    $field = array(
      'field_name' => $key,
      'type' => $field_type,
      'entity_types' => array($entity),
    );
    field_create_field($field);
  }
  else {
    if (!formio_bundle_field_has_data($field, $bundle)) {
      field_update_field($field);
    }
  }
  if (empty($instance)) {
    $instance = array(
      'label' => $label,
      'widget' => array('type' => $widget_type),
      'settings' => $settings,
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'text_default',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'text_default',
        ),
      ),
      'required' => (isset($component['validate']) && $component['validate']['required']) ? TRUE : FALSE,
      'default_value' => (isset($component['defaultValue'])) ? $component['defaultValue'] : NULL,
      'field_name' => $key,
      'entity_type' => $entity,
      'bundle' => $bundle,
    );
    $instance = field_create_instance($instance);
  }
  else {
    // If field has no data allow updating.
    if (!formio_bundle_field_has_data($field, $bundle)) {
      field_update_instance($instance);
    }
  }
  return $instance;
}
