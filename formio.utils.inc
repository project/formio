<?php
/**
 * @file
 * Contains utility functions for the formio module.
 */

/**
 * Get a specific form directly from Form.io.
 *
 * @param string $_id
 *   The unique string that represents a Formio form.
 * @param bool $reset
 *   Skip static cache.
 *
 * @return object
 *   The form object.
 */
function formio_get_form($_id, $reset = FALSE, $json = FALSE) {
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast = &drupal_static(__FUNCTION__);
  }
  $forms = &$drupal_static_fast;
  if (!$forms) {
    $forms = array();
  }

  if ($reset || !isset($forms[$_id])) {
    $formio = formio_rest('form/' . $_id, $json);
  }

  return $formio->result;
}

/**
 * Get defined forms from the database.
 *
 * @return mixed
 */
function formio_load_forms($column, $value) {
  $forms = db_select('formio_form', 'ff')
    ->fields('ff')
    ->condition('ff.' . $column, $value)
    ->execute()
    ->fetchAllAssoc('drupal_name');

  return $forms;
}

/**
 * Determine whether a field has any data.
 *
 * This is basically field_has_data() with a slight modification.
 *
 * @param $field
 *   A field structure.
 * @param string $bundle
 *   The entity bundle.
 *
 * @return bool
 *   TRUE if the field, for a particular bundle, has data. FALSE otherwise.
 */
function formio_bundle_field_has_data($field, $bundle) {
  $query = new EntityFieldQuery();
  $query = $query->fieldCondition($field)
    ->entityCondition('bundle', $bundle)
    ->range(0, 1)
    ->count()
    // Neutralize the 'entity_field_access' query tag added by
    // field_sql_storage_field_storage_query(). The result cannot depend on the
    // access grants of the current user.
    ->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');

  $result = $query->execute();
  return (bool) $result;
}

/**
 * The label callback for a formio entity.
 *
 * @param object $entity
 * @param string $entity_type
 * @return string
 */
function formio_entity_label_callback($entity, $entity_type) {
  // @todo: Really implement this.
  $info = $entity->entityInfo();
  $bundle = $info['entity keys']['bundle'];
  $label = $info['bundles'][$bundle]['label'];
  return $label;
}

/**
 * Render a formio form.
 *
 * @param array $conf
 */
function formio_render($formio, $settings = NULL) {

  // NEW...
  if (isset($formio->_id)) {
    // Cast to an array.
    $vars['form'] = (array) $formio;
    return theme('formio_form', $vars);
  }
  else {
    return 'No forms found';
  }

  // END NEW...
  // Params needed for ctools. Building them out so we know what they are.
//  $type = 'formio_form';
//  $subtype = $formio->drupal_name;
//  $args = array();
//  $keywords = $contexts = array();
//
//  ctools_include('content');
//  $formio = formio_load_drupal_form($subtype);
//  $conf = (array) $formio;
//  $renderer = ctools_content_render($type, $subtype, $conf, $keywords, $args, $contexts, 'hello');
//
//  // Called second on blocks.
//
//  // Include the seamless library. THIS HAS TO BE HERE.
//  drupal_add_library('formio', 'seamless');
//
//  // Render the html in the Formio fieldset. @todo: Fix this render
//  return theme('formio_form', $renderer->content);
}

/**
 * Attaching headers to the render array.
 *
 * @return array
 *   An array of headers.
 */
function formio_add_headers() {
  $apiKey = formio_apikey();
  if (empty($apiKey)) {
    throw new Exception('You must have an api key to continue.');
  }
  return array(
    array('x-token', $apiKey),
    array('Accept', 'application/json'),
    array('Access-Control-Request-Method', 'POST, GET, OPTIONS'),
  );
}

/**
 * Creates a new exportable object for the ctools content type.
 *
 * This ensures we get proper defaults from the plugin for its settings.
 */
function formio_new_content_type($set_defaults) {
  $item = ctools_export_new_object('formio_form', $set_defaults);
  ctools_include('content');
  $plugin = ctools_get_content_type('formio_form');
  $item->settings = ctools_content_get_defaults($plugin, array());
  return $item;
}

/**
 * Combines form values from the Form.io form and the form_state into the item.
 *
 * The item is the exportable object.
 *
 * @param $formio
 * @param $item
 * @param $form_state
 * @param $reusable
 */
function formio_form_combine_values($formio, $item, &$form_state, $reusable) {

  // Add the machine name.
  $item->drupal_name = $form_state['values']['drupal_name'];

  foreach (array_keys($formio) as $key) {
    if ($key === 'modified') {
      $value = (new DateTime($formio['modified']))->getTimestamp();
    }
    else if ($key === 'created') {
      $value = (new DateTime($formio['created']))->getTimestamp();
    }
    else {
      $value = $formio[$key];
    }
    $item->{$key} = $value;
  }
  // Save default values for the form_state conf and the item.
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    $form_state['conf'][$key] = $item->{$key} = isset($form_state['values'][$key]) ? $form_state['values'][$key] : $form_state['plugin']['defaults'][$key];
  }
  // Save customizations for the form_state conf and the item.
  foreach (array_keys(formio_get_extra_settings()) as $key) {
    $form_state['conf'][$key] = $item->settings[$key] = isset($form_state['values'][$key]) ? $form_state['values'][$key] : '';
  }
}

/**
 * Filter out the values that are not custom.
 *
 * @param array $values
 *   The values to filter.
 *
 * @return array
 *   Filtered values.
 */
function formio_filter_values(&$values) {
  $vars = array();
  $settings = formio_get_extra_settings();
  foreach ($settings as $key => $setting) {
    $vars[$key] = isset($values[$key]) ? $values[$key] : $setting['form']['#default_value'];
    unset($values[$key]);
  }
  return $vars;
}

/**
 * Checks to see if a form with the same 'drupal_name' already exists.
 *
 * @see menu_edit_menu()
 * @see form_validate_machine_nxame()
 */
function formio_form_name_exists($drupal_name, $element, &$form_state) {
  $name_exists = db_query_range('SELECT 1 FROM {formio_form} WHERE drupal_name = :drupal_name', 0, 1, array(':drupal_name' => $drupal_name))->fetchField();
  return $name_exists;
}

/**
 * Convert formio field name to drupal field name.
 */
function formio_convert_formio_field_to_drupal_field($name) {
  return 'formio_' . strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $name));
}

/**
 * Helper to return the correct class based on the api.
 */
function formio_api_class() {
  return 'Drupal\formio\Plugin\resource\formio\forms\Forms__' . FORMIO_API_MAJOR . '_' . FORMIO_API_MINOR;
}

/**
 * Helper to get the api key.
 *
 * @return string
 *   The api key registered with Form.io
 */
function formio_apikey() {
  return Formio::apiKey();
}
